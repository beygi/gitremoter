#gitRemoter

##installation
* create rsa key in client and server and add it to your bitbucket/github/git account  
if you are not alone in server we recomend to create rsa key with **password** in server
* copy **git-rpush** to any of your **PATH**
* copy **remote-update.sh** to **`.git/hooks/`** in your projects directory
* modify **remote-update.sh** :  
	+ change **username** and **host** for ssh
	+ change your **project directory** after cd

##usage
use `git rpush` instead of `git push` will push to server and update remote directory
